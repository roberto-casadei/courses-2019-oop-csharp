﻿using System;

namespace PartOne
{
    abstract class FormaGeometricaPiana
    {
        public abstract int CalcolaPerimetro();
    }

    class Quadrato : FormaGeometricaPiana
    {
        public override int CalcolaPerimetro()
        {
            return 0;
        }
    }
}