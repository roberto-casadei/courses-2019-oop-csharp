﻿using System;

namespace it.unibo.oop.example01.slide12
{
    class Slide_16
    {
        /*
        * Main method
        * Application Entry-Point
        */
        public static void Main(string[] args)
        {
            //Print a message on the StdOut
            Console.WriteLine("Hello, World!");
        }
    }
}